package adamantpenguin.serverscanner;

import eu.midnightdust.lib.config.MidnightConfig;
import net.fabricmc.api.ClientModInitializer;

public class ServerScannerInit implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        MidnightConfig.init("serverscanner", ScannerConfig.class);
    }
}
