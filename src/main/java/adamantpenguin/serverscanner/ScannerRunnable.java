package adamantpenguin.serverscanner;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.option.ServerList;
import net.minecraft.client.toast.SystemToast;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.NetworkState;
import net.minecraft.network.listener.ClientQueryPacketListener;
import net.minecraft.network.packet.c2s.handshake.HandshakeC2SPacket;
import net.minecraft.network.packet.c2s.query.QueryRequestC2SPacket;
import net.minecraft.network.packet.s2c.query.QueryPongS2CPacket;
import net.minecraft.network.packet.s2c.query.QueryResponseS2CPacket;
import net.minecraft.text.Text;

import java.net.InetSocketAddress;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class ScannerRunnable implements Runnable {
    ButtonWidget[] buttons;
    MinecraftClient client;
    ServerList serverList;
    Runnable onFinished;
    public ScannerRunnable(ButtonWidget[] buttons, ServerList serverList, Runnable onFinished) {
        this.buttons = buttons;
        this.serverList = serverList;
        this.onFinished = onFinished;
        client = MinecraftClient.getInstance();
    }

    @Override
    public void run() {
        // disable buttons
        for (ButtonWidget button : buttons) {
            button.active = false;
        }

        // notify user that we're now launching zmap
        client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                Text.translatable("serverscanner.toast.scanning.title"),
                Text.translatable("serverscanner.toast.scanning.description")));

        // get IP addresses from zmap
        ProcessBuilder zmapBuilder = new ProcessBuilder();
        zmapBuilder.command(ScannerConfig.zmapBinary,
                "--target-port=25565",
                "--bandwidth=" + ScannerConfig.maxBandwidth,
                "--max-results=" + ScannerConfig.batchSize,
                "--quiet", "--verbosity=0");
        try {
            Process zmapProcess = zmapBuilder.start();
            Stream<String> addresses = new String(zmapProcess.getInputStream().readAllBytes()).lines();
            zmapProcess.waitFor();

            // notify user that we're now pinging servers
            client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                    Text.translatable("serverscanner.toast.checking.title"),
                    Text.translatable("serverscanner.toast.checking.description")));

            // ping/add each server
            addresses.forEach(this::pingAndAddServer);

            // notify user that we're done
            client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                    Text.translatable("serverscanner.toast.success.title"),
                    Text.translatable("serverscanner.toast.success.description")));

            // execute callback
            client.execute(onFinished);
        } catch (Exception e) {
            // notify user of failure
            client.getToastManager().add(new SystemToast(SystemToast.Type.PERIODIC_NOTIFICATION,
                    Text.translatable("serverscanner.toast.failure.title"),
                    Text.literal(e.getClass().getSimpleName() + ": " + e.getLocalizedMessage())));
        }

        // re-enable buttons
        for (ButtonWidget button : buttons) {
            button.active = true;
        }
    }

    private void pingAndAddServer(String address) {
        // ping the server
        try {
            ClientConnection connection = ClientConnection.connect(new InetSocketAddress(address, 25565), false);
            connection.setPacketListener(new ClientQueryPacketListener() {
                @Override
                public void onResponse(QueryResponseS2CPacket packet) {
                    // on any response, add to server list and disconnect
                    serverList.add(new ServerInfo(address, address, false), false);
                    connection.disconnect(Text.translatable("multiplayer.status.finished"));
                }

                @Override
                public void onPong(QueryPongS2CPacket packet) {}

                @Override
                public void onDisconnected(Text reason) {}

                @Override
                public ClientConnection getConnection() {
                    return connection;
                }
            });

            connection.send(new HandshakeC2SPacket(address, 25565, NetworkState.STATUS));
            connection.send(new QueryRequestC2SPacket());
        } catch (Exception ignored) {}
    }
}
