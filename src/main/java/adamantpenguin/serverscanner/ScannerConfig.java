package adamantpenguin.serverscanner;

import eu.midnightdust.lib.config.MidnightConfig;

public class ScannerConfig extends MidnightConfig {
    @Entry public static String zmapBinary = "zmap";
    @Entry public static String maxBandwidth = "10M";
    @Entry public static int batchSize = 50;
}
