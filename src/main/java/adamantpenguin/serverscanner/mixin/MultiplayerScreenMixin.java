package adamantpenguin.serverscanner.mixin;

import adamantpenguin.serverscanner.ScannerRunnable;
import eu.midnightdust.lib.config.MidnightConfig;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.client.option.ServerList;
import net.minecraft.text.Text;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MultiplayerScreen.class)
public abstract class MultiplayerScreenMixin extends Screen {
    @Shadow protected abstract void init();

    @Shadow private ServerList serverList;

    @Shadow protected abstract void refresh();

    protected MultiplayerScreenMixin(Text title) {
        super(title);
    }

    @Inject(at = @At(value = "RETURN"), method = "init")
    public void addButton(CallbackInfo ci) {
        ButtonWidget configButton = new ButtonWidget(width - 30, 5, 25, 20, Text.literal("…"), button -> {
            client.setScreen(MidnightConfig.getScreen(this, "serverscanner"));
        });
        addDrawableChild(configButton);
        addDrawableChild(new ButtonWidget(width - 80, 5, 50, 20, Text.translatable("serverscanner.gui.scan"),
                button -> {
                    // run server scanner
                    Thread scannerThread = new Thread(new ScannerRunnable(new ButtonWidget[]{button, configButton},
                            serverList, () -> {
                        serverList.saveFile();
                        refresh();
                    }));
                    scannerThread.setName("Server scanner thread");
                    scannerThread.start();
        }));
    }
}
