# Server scanner
Scan the Internet for Minecraft servers within the multiplayer menu

Requires [ZMap](https://github.com/zmap/zmap) to be installed on your system.  You may be able to find it in your
package repositories, or you could download a binary from their GitHub releases, or build it from source.

Currently compatible with Minecraft Java Edition 1.19.*